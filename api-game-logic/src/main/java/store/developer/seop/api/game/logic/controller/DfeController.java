package store.developer.seop.api.game.logic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.game.logic.model.DfeItem;
import store.developer.seop.api.game.logic.model.DfeRequest;
import store.developer.seop.api.game.logic.model.DfeUpdateRequest;
import store.developer.seop.api.game.logic.service.DefService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "Df 엔티티 관리")
@RequestMapping("/v1/dfe")
@RestController
@RequiredArgsConstructor
public class DfeController {
    private final DefService defService;

    @ApiOperation(value = "df 엔티티 등록")
    @PostMapping("/new")
    public CommonResult setDfe(@RequestBody @Valid DfeRequest request) {
        defService.setDfe(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "df 엔티티 목록")
    @GetMapping("/list")
    public ListResult<DfeItem> getDfes() {
        return ResponseService.getListResult(defService.getDfe(), true);
    }
    @ApiOperation(value = "df 엔티티 수정")
    @PutMapping("/data/{id}")
    public CommonResult putDfe(@PathVariable("id") long id, @RequestBody @Valid DfeUpdateRequest request) {
        defService.putDfe(id, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "df 엔티티 삭제")
    @DeleteMapping("/data/{id}")
    public CommonResult delDfe(@PathVariable("id") long id) {
        defService.delDfe(id);

        return ResponseService.getSuccessResult();
    }
}
