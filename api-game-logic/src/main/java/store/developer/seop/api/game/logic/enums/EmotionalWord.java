package store.developer.seop.api.game.logic.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmotionalWord {
    POSITIVE("긍정", 10)
    , DENIAL("부정", -10)
    , NEUTRALITY("중립", 0)

    ;
    private final String emotionName;
    private final Integer emotionFigure;
}
