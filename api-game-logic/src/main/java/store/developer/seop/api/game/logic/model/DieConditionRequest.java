package store.developer.seop.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DieConditionRequest {
    @ApiModelProperty(notes = "인텐트 이름(2~100글자)")
    @NotNull
    @Length(min = 2, max = 100)
    private String intentName;
}
