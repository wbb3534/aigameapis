package store.developer.seop.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.game.logic.entity.DieCondition;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import javax.persistence.Column;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DieConditionItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;
    @ApiModelProperty(notes = "받는 인텐트 이름")
    private String intentName;

    private DieConditionItem(DieConditionItemBuilder builder) {
        this.id = builder.id;
        this.intentName = builder.intentName;
    }
    public static class DieConditionItemBuilder implements CommonModelBuilder<DieConditionItem> {
        private final Long id;
        private final String intentName;

        public DieConditionItemBuilder(DieCondition dieCondition) {
            this.id = dieCondition.getId();
            this.intentName = dieCondition.getIntentName();
        }
        @Override
        public DieConditionItem build() {
            return new DieConditionItem(this);
        }
    }
}
