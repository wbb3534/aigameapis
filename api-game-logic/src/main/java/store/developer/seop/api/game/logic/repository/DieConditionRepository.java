package store.developer.seop.api.game.logic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import store.developer.seop.api.game.logic.entity.DieCondition;

import java.util.List;

public interface DieConditionRepository extends JpaRepository<DieCondition, Long> {
    List<DieCondition> findAllByIdGreaterThanEqualOrderByIdAsc(long id);

    long countByIntentName(String intentName);
}
