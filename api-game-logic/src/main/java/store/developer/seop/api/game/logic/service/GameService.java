package store.developer.seop.api.game.logic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.game.logic.entity.Dfe;
import store.developer.seop.api.game.logic.model.GameRequest;
import store.developer.seop.api.game.logic.model.GameResponse;
import store.developer.seop.api.game.logic.repository.DfeRepository;
import store.developer.seop.api.game.logic.repository.DieConditionRepository;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GameService {
    private final DieConditionRepository dieConditionRepository;
    private final DfeRepository dfeRepository;

    public GameResponse doCalculation(GameRequest gameRequest) {
        // todo: api 연동하기
        // api 연동 전 임시데이터
        String humanQueryText = "숙취";
        String aiFulfillmentText = "숙취 해장엔 역시 술이지~";
        List<String> tempParmaKeys = new LinkedList<>();
        tempParmaKeys.add("c_food");
        tempParmaKeys.add("c_dance");

        // gameRequest에서 message를 빼다가 df에게 전해준다 --> df response 받기

        // ----- 즉사여부 확인 start -----
        // df response에서 intent - displayName을 빼온다.
        String tempDisplayName = "do_hangover"; // todo : 임시값 api 연동되면 교체
        // DieCondition에서 위에서 가져온 displayName에 해당하는 값이 있는지 확인한다. --> count로 확인
        long intentDuplicateCount = dieConditionRepository.countByIntentName(tempDisplayName);
        // 만약 데이터가 있으면 즉사처리 데이터가 없으면 2단계로 진행

        if (intentDuplicateCount > 0) {
            return new GameResponse.GameResponseBuilder(humanQueryText, aiFulfillmentText, true, 0).build();
        } else {
            return new GameResponse.GameResponseBuilder(humanQueryText, aiFulfillmentText, false, calculateChangeVitalityFigures(tempParmaKeys)).build();
        }
        // ----- 즉사여부 확인 end -----
    }

    private int calculateChangeVitalityFigures(List<String> parmaKeys) {
        List<Dfe> dfes = new LinkedList<>();

        // 내가 가진 키들의 상세 데이터를 dfes에 모은다.
        for (String key : parmaKeys) {
            dfes.add(dfeRepository.findByDfeName(key));
        }

        int kindValue = 0;
        int figureChangeCount = 0;
        for (Dfe dfe : dfes) {
            kindValue += dfe.getEmotionalWord().getEmotionFigure();
            if (dfe.getIsFigureChange()) {
                figureChangeCount += 1;
            }
        }
        return kindValue * figureChangeCount;
    }
}
