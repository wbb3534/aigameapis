package store.developer.seop.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import store.developer.seop.api.game.logic.enums.EmotionalWord;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DfeUpdateRequest {
    @ApiModelProperty(notes = "df 엔티티 이름(~50글자)")
    @NotNull
    @Length(max = 50)
    private String dfeName;
    @ApiModelProperty(notes = "감정말(긍정,부정,중립) enum")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private EmotionalWord emotionalWord;
    @ApiModelProperty(notes = "수치변화 여부")
    @NotNull
    private Boolean isFigureChange;
}
