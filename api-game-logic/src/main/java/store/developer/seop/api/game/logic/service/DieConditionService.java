package store.developer.seop.api.game.logic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.game.logic.entity.DieCondition;
import store.developer.seop.api.game.logic.model.DieConditionItem;
import store.developer.seop.api.game.logic.model.DieConditionRequest;
import store.developer.seop.api.game.logic.model.DieConditionUpdateRequest;
import store.developer.seop.api.game.logic.repository.DieConditionRepository;
import store.developer.seop.common.exception.CMissingDataException;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ListConvertService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DieConditionService {
    private final DieConditionRepository dieConditionRepository;

    public void setDieCondition(DieConditionRequest request) {
        DieCondition addData = new DieCondition.DieConditionBuilder(request).build();

        dieConditionRepository.save(addData);
    }

    public ListResult<DieConditionItem> getDieConditions() {
        List<DieCondition> originList = dieConditionRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L);

        List<DieConditionItem> result = new ArrayList<>();

        originList.forEach(item -> result.add(new DieConditionItem.DieConditionItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putDieCondition(long id, DieConditionUpdateRequest request) {
       DieCondition originData = dieConditionRepository.findById(id).orElseThrow(CMissingDataException::new);

       originData.putDieCondition(request);

       dieConditionRepository.save(originData);
    }

    public void delDieCondition(long id) {
        dieConditionRepository.deleteById(id);
    }
}
