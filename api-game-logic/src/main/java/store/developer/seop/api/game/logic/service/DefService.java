package store.developer.seop.api.game.logic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import store.developer.seop.api.game.logic.entity.Dfe;
import store.developer.seop.api.game.logic.model.DfeItem;
import store.developer.seop.api.game.logic.model.DfeRequest;
import store.developer.seop.api.game.logic.model.DfeUpdateRequest;
import store.developer.seop.api.game.logic.repository.DfeRepository;
import store.developer.seop.common.exception.CMissingDataException;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ListConvertService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DefService {
    private final DfeRepository dfeRepository;

    public void setDfe(DfeRequest request) {
        Dfe addData = new Dfe.dfeBuilder(request).build();

        dfeRepository.save(addData);
    }

    public ListResult<DfeItem> getDfe() {
        List<Dfe> originList = dfeRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L);

        List<DfeItem> result = new ArrayList<>();

        originList.forEach(item -> result.add(new DfeItem.DfeItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putDfe(long id, DfeUpdateRequest request) {
        Dfe originData = dfeRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putDfe(request);

        dfeRepository.save(originData);
    }

    public void delDfe(long id) {
        dfeRepository.deleteById(id);
    }
}
