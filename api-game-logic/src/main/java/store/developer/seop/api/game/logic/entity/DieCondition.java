package store.developer.seop.api.game.logic.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.game.logic.model.DieConditionRequest;
import store.developer.seop.api.game.logic.model.DieConditionUpdateRequest;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DieCondition {
    @ApiModelProperty(notes = "즉사 시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "인텐트 이름")
    @Column(nullable = false, length = 100)
    private String intentName;

    public void putDieCondition(DieConditionUpdateRequest request) {
        this.intentName = request.getIntentName();
    }
    private DieCondition(DieConditionBuilder builder) {
        this.intentName = builder.intentName;
    }
    public static class DieConditionBuilder implements CommonModelBuilder<DieCondition> {
        private final String intentName;

        public DieConditionBuilder(DieConditionRequest request) {
            this.intentName = request.getIntentName();
        }
        @Override
        public DieCondition build() {
            return new DieCondition(this);
        }
    }
}
