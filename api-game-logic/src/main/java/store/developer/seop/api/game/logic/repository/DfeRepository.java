package store.developer.seop.api.game.logic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import store.developer.seop.api.game.logic.entity.Dfe;
import store.developer.seop.api.game.logic.entity.DieCondition;

import java.util.List;

public interface DfeRepository extends JpaRepository<Dfe, Long> {
    List<Dfe> findAllByIdGreaterThanEqualOrderByIdAsc(long id);
    Dfe findByDfeName(String dfeName);
}
