package store.developer.seop.api.game.logic.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import store.developer.seop.api.game.logic.model.DieConditionItem;
import store.developer.seop.api.game.logic.model.DieConditionRequest;
import store.developer.seop.api.game.logic.model.DieConditionUpdateRequest;
import store.developer.seop.api.game.logic.service.DieConditionService;
import store.developer.seop.common.response.model.CommonResult;
import store.developer.seop.common.response.model.ListResult;
import store.developer.seop.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "즉사조건 관리")
@RequestMapping("/v1/die-condition")
@RequiredArgsConstructor
@RestController
public class DieConditionController {
    private final DieConditionService dieConditionService;

    @ApiOperation(value = "즉사조건 등록")
    @PostMapping("/new")
    public CommonResult setDieCondition(@RequestBody @Valid DieConditionRequest request) {
        dieConditionService.setDieCondition(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "즉사조건 조회")
    @GetMapping("/list")
    public ListResult<DieConditionItem> getDieConditions() {
        return ResponseService.getListResult(dieConditionService.getDieConditions(), true);
    }

    @ApiOperation(value = "즉사조건 수정")
    @PutMapping("/data/{id}")
    public CommonResult putDieCondition(@PathVariable("id") long id, @RequestBody @Valid DieConditionUpdateRequest request) {
        dieConditionService.putDieCondition(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "즉사조건 삭제")
    @DeleteMapping("/data/{id}")
    public CommonResult delDieCondition(@PathVariable("id") long id) {
        dieConditionService.delDieCondition(id);

        return ResponseService.getSuccessResult();
    }
}
