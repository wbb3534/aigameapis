package store.developer.seop.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GameRequest {
    @ApiModelProperty(notes = "개복치가 사람에게 한말")
    @NotNull
    @Length(min = 5, max = 50)
    private String message;
}
