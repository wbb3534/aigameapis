package store.developer.seop.api.game.logic.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.game.logic.enums.EmotionalWord;
import store.developer.seop.api.game.logic.model.DfeRequest;
import store.developer.seop.api.game.logic.model.DfeUpdateRequest;
import store.developer.seop.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Dfe {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(notes = "df 엔티티 이름")
    @Column(nullable = false, length = 50)
    private String dfeName;
    @ApiModelProperty(notes = "감정말(긍정,부정,중립) enum")
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private EmotionalWord emotionalWord;
    @ApiModelProperty(notes = "수치변화 여부")
    @Column(nullable = false)
    private Boolean isFigureChange;

    public void putDfe(DfeUpdateRequest request) {
        this.dfeName = request.getDfeName();
        this.emotionalWord = request.getEmotionalWord();
        this.isFigureChange = getIsFigureChange();
    }

    private Dfe(dfeBuilder builder) {
        this.dfeName = builder.dfeName;
        this.emotionalWord = builder.emotionalWord;
        this.isFigureChange = builder.isFigureChange;

    }
    public static class dfeBuilder implements CommonModelBuilder<Dfe> {
        private final String dfeName;
        private final EmotionalWord emotionalWord;
        private final Boolean isFigureChange;

        public dfeBuilder(DfeRequest request) {
            this.dfeName = request.getDfeName();
            this.emotionalWord = request.getEmotionalWord();
            this.isFigureChange = request.getIsFigureChange();
        }
        @Override
        public Dfe build() {
            return new Dfe(this);
        }
    }
}
