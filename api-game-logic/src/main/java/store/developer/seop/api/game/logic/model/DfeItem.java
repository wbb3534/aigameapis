package store.developer.seop.api.game.logic.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import store.developer.seop.api.game.logic.entity.Dfe;
import store.developer.seop.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DfeItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;
    @ApiModelProperty(notes = "df 엔티티 이름")
    private String dfeName;
    @ApiModelProperty(notes = "감정말(긍정,부정,중립) 이름")
    private String emotionalWordName;
    @ApiModelProperty(notes = "감정말(긍정,부정,중립) 수치")
    private Integer emotionFigure;
    @ApiModelProperty(notes = "수치변화 여부")
    private Boolean isFigureChange;

    private DfeItem(DfeItemBuilder builder) {
        this.id = builder.id;
        this.dfeName = builder.dfeName;
        this.emotionalWordName = builder.emotionalWordName;;
        this.emotionFigure = builder.emotionFigure;
        this.isFigureChange = builder.isFigureChange;

    }
    public static class DfeItemBuilder implements CommonModelBuilder<DfeItem> {
        private final Long id;
        private final String dfeName;
        private final String emotionalWordName;
        private final Integer emotionFigure;
        private final Boolean isFigureChange;

        public DfeItemBuilder(Dfe dfe) {
            this.id = dfe.getId();
            this.dfeName = dfe.getDfeName();
            this.emotionalWordName = dfe.getEmotionalWord().getEmotionName();
            this.emotionFigure = dfe.getEmotionalWord().getEmotionFigure();
            this.isFigureChange = dfe.getIsFigureChange();
        }
        @Override
        public DfeItem build() {
            return new DfeItem(this);
        }
    }
}
