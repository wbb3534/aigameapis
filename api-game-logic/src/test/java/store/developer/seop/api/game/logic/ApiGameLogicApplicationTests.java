package store.developer.seop.api.game.logic;

import com.google.cloud.dialogflow.v2.QueryResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import store.developer.seop.api.game.logic.service.DialogFlowService;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class ApiGameLogicApplicationTests {

    @Autowired
    DialogFlowService dialogFlowService;

    @Test
    void contextLoads() throws IOException {
        String projectId = "plenary-line-379101";
        List<String> texts = new LinkedList<>();
        texts.add("안녕");
        String sessionId = "123456789";
        String langCode = "ko-KR";

        Map<String, QueryResult> result = dialogFlowService.detectIntentTexts(projectId, texts, sessionId, langCode);
        int a = 1;
    }

}
